$(document).ready(function () {

    $("#btnCalculImc").click(function () {

        var poids = $("#idPoids").val();
        poids = poids.replace(",", ".");
        poids = Number(poids);

        var taille = $("#idTaille").val();
        taille = taille.replace(",", ".");
        taille = Number(taille);

        if (isNaN(poids) || isNaN(taille)) {

            $("#textIMC").html("Saisie incorrecte") ;

        } else {

            var imc = calculerIMC(poids, taille / 100) ;   
            var texte = imc.toFixed(1) ;

            texte += "(" + interpretationIMC(imc) + ")" ;              
            $("#textIMC").html(texte) ;

        }


    }) ;


    function calculerIMC(prmPoids, prmTaille){

        var valRetour = prmPoids / (prmTaille * prmTaille);

        return valRetour;
    }

    function interpretationIMC(prmValIMC){

        interpretation = "" ;

        if (prmValIMC < 16.5){
            interpretation = "Dénutrition" ;
        } else if (prmValIMC < 18.5){
            interpretation = "Maigreur" ;
        } else if (prmValIMC < 25){
            interpretation = "Corpulence normale" ;
        } else if (prmValIMC < 30){
            interpretation = "Surpoids" ;
        } else if (prmValIMC < 35){
            interpretation = "Obésité modérée" ;
        } else if (prmValIMC < 40){
            interpretation = "Obésité sévère" ;
        } else if (prmValIMC >= 40){
            interpretation = "Obésité morbide" ;
        }

        return interpretation ;
    }

 }) ;