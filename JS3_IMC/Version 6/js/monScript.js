$(document).ready(function() {
  //------------------------------//
  //  déclarations de variables   //
  //------------------------------//

  //------------------------------//
  //     programme principal      //
  //------------------------------//

  //------------------------------//
  //  gestionnaires d'évènements  //
  //------------------------------//

  $("#idSliderPoids").on("input", function() {
    afficherIMC();
  });

  $("#idSliderTaille").on("input", function() {
    afficherIMC();
  });

  $("input[name=sexe]").on("change", function() {
    var sexe = $("input[name=sexe]:checked").val();

    if (sexe == "homme") {
      $("#silhouette").css("background-image", "url(images/IMC-homme.jpg)");
    } else {
      $("#silhouette").css("background-image", "url(images/IMC-femme.jpg)");
    }

    afficherIMC();
  });

  //------------------------------//
  //  déclaration des fonctions   //
  //------------------------------//

  function afficherIMC() {
    var poids = $("#idSliderPoids").val();
    var taille = $("#idSliderTaille").val();

    poids = poids.replace(",", ".");
    taille = taille.replace(",", ".");

    poids = Number(poids);
    taille = Number(taille);

    $("#textPoids").html(poids);
    $("#textTaille").html(taille);

    var imc = calculerIMC(poids, taille / 100);
    var texte = imc.toFixed(1);

    texte += "(" + interpretationIMC(imc) + ")";
    $("#textIMC").html(texte);

    afficherBalance(imc);

    afficherSilhouette(imc);
  }

  function calculerIMC(prmPoids, prmTaille) {
    var valRetour = prmPoids / (prmTaille * prmTaille);

    return valRetour;
  }

  function interpretationIMC(prmValIMC) {
    interpretation = "";

    if (prmValIMC < 16.5) {
      interpretation = "Dénutrition";
    } else if (prmValIMC < 18.5) {
      interpretation = "Maigreur";
    } else if (prmValIMC < 25) {
      interpretation = "Corpulence normale";
    } else if (prmValIMC < 30) {
      interpretation = "Surpoids";
    } else if (prmValIMC < 35) {
      interpretation = "Obésité modérée";
    } else if (prmValIMC < 40) {
      interpretation = "Obésité sévère";
    } else if (prmValIMC >= 40) {
      interpretation = "Obésité morbide";
    }

    return interpretation;
  }

  function afficherBalance(prmValImc) {
    var deplacement;

    if (prmValImc < 10) {
      prmValImc = 10;
    }
    if (prmValImc > 45) {
      prmValImc = 45;
    }

    deplacement = (300 / 35) * prmValImc - 600 / 7;

    $("#aiguille").css("left", deplacement + "px");
  }

  function afficherSilhouette(prmValImc) {
    var decalage = "";

    if (prmValImc < 16.5) {
      decalage = 0;
    } else if (prmValImc < 18.5) {
      decalage = -105;
    } else if (prmValImc < 25) {
      decalage = -210;
    } else if (prmValImc < 30) {
      decalage = -315;
    } else if (prmValImc < 35) {
      decalage = -420;
    } else if (prmValImc >= 35) {
      decalage = -525;
    }

    $("#silhouette").css("background-position", decalage);
  }
});
