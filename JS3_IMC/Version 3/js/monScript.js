$(document).ready(function () {

    $("#idSliderPoids").on('input', function () {

        afficherIMC() ;

    }) ;

    $("#idSliderTaille").on('input', function () {

        afficherIMC() ;

    }) ;


    function afficherIMC(){

        var poids = $("#idSliderPoids").val();
        var taille = $("#idSliderTaille").val();

        poids = poids.replace(",", ".");
        taille = taille.replace(",", ".");   
           
        poids = Number(poids);
        taille = Number(taille);

        $("#textPoids").html(poids) ;        
        $("#textTaille").html(taille) ;

        var imc = calculerIMC(poids, taille / 100) ;   
        var texte = imc.toFixed(1) ;

        texte += "(" + interpretationIMC(imc) + ")" ;              
        $("#textIMC").html(texte) ;        
    }

    function calculerIMC(prmPoids, prmTaille){

        var valRetour = prmPoids / (prmTaille * prmTaille);

        return valRetour;
    }

    function interpretationIMC(prmValIMC){

        interpretation = "" ;

        if (prmValIMC < 16.5){
            interpretation = "Dénutrition" ;
        } else if (prmValIMC < 18.5){
            interpretation = "Maigreur" ;
        } else if (prmValIMC < 25){
            interpretation = "Corpulence normale" ;
        } else if (prmValIMC < 30){
            interpretation = "Surpoids" ;
        } else if (prmValIMC < 35){
            interpretation = "Obésité modérée" ;
        } else if (prmValIMC < 40){
            interpretation = "Obésité sévère" ;
        } else if (prmValIMC >= 40){
            interpretation = "Obésité morbide" ;
        }

        return interpretation ;
    }


 }) ;