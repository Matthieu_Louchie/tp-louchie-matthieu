$(document).ready(function () {

    $("#btnCalculImc").click(function () {

        var poids = $("#idPoids").val();
        poids = poids.replace(",", ".");
        poids = Number(poids);

        var taille = $("#idTaille").val();
        taille = taille.replace(",", ".");
        taille = Number(taille);

        if (isNaN(poids) || isNaN(taille)) {

            $("#textIMC").html("Saisie incorrecte") ;

        } else {

            var imc = calculerIMC(poids, taille / 100) ;   
            var texte = imc.toFixed(1) ;
           
            $("#textIMC").html(texte) ;

        }


    }) ;


    function calculerIMC(prmPoids, prmTaille){

        var valRetour = prmPoids / (prmTaille * prmTaille);

        return valRetour;
    }

 }) ;