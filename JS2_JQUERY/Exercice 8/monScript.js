$(document).ready(function () {                                             
    $("#total").html(calculTotal());                                        
    
    
    $("#BtnAjouter").click(function () {                                    
        let longueurliste = $("tbody tr").length + 1;                       
        $("tbody").append("<tr><td>" + longueurliste + "</td></tr>");       
        $("#total").html(calculTotal());                                    
    })


    $("#BtnSupprimer").click(function () {                       
        $("tbody tr:last-child").remove();                                 
        $("#total").html(calculTotal());                                   
    })

    
    function calculTotal() {                                              
        let somme = 0;                                                   
        $("tbody td").each(function (index) {                               
            somme += parseInt($(this).html());                              
        })
        return somme;                                                       
    }
});
