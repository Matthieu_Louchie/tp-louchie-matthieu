$(document).ready(function () {

    let divAffichee = false;

    $("#btn1").click(function () {

        if (divAffichee === false) {
            //afficher la DIV
            $("#maDiv1").show("slow");
            $("#btn1").attr("value", "Fermer");
        } else {
            //cacher la DIV
            $("#maDiv1").hide("slow");
            $("#btn1").attr("value", "Ouvrir");
        }
 
        divAffichee = !divAffichee;

    });

});